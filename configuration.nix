{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      ./common/gtk.nix
      ./common/users.nix
      ./common/steam-controller.nix
      ./common/cyborg-rat3.nix
    ];

  boot = {
    loader = {
      efi.efiSysMountPoint = "/boot";
      timeout = 1;
      grub = {
        enable = true;
        device = "nodev";
        efiSupport = true;
        efiInstallAsRemovable = true;
        configurationLimit = 10;
      };
    };

    initrd.availableKernelModules = [
      "ahci"
      "usb_storage"
    ];

    kernelModules = [
      "fbcon"
      "kvm-intel"
    ];
  };

  boot.tmpOnTmpfs = true;

  networking = {
    hostName = "thinkpad";
    networkmanager.enable = true;
  };

  i18n = {
    consoleKeyMap = "de";
    defaultLocale = "de_DE.UTF-8";
  };

  nix = {
    extraOptions = ''
      binary-caches-parallel-connections = 40
    '';
  };

  nixpkgs.config.allowUnfree = true;

  time.timeZone = "Europe/Berlin";

  environment = {
    systemPackages = with pkgs; [

      zsh nix-zsh-completions

      coreutils curl file
      xdg-user-dirs xdg_utils which
      ftop htop psmisc
      wget mosh udevil
      neovim
      
      ntfs3g bashmount
      
      networkmanagerapplet blueman cbatticon

      feh imagemagick scrot

      i3-gaps i3lock tree
      dmenu2 dunst libnotify

      compton xflux nitrogen

      paprefs pavucontrol pamixer
      volumeicon

      ntfs3g bashmount

      arandr xorg.xrandr
    ];

    variables = {
      SUDO_ASKPASS = "${pkgs.x11_ssh_askpass}/libexec/x11-ssh-askpass";
      SDL_VIDEO_X11_DGAMOUSE = "0";
    };

    shellAliases = {
      ednixconf = "sudo $EDITOR /etc/nixos";
      nixrbs = "sudo nixos-rebuild switch";
      nixpkgup = "nix-env -u";
      nixpkgtup = "nix-env -u --dry-run";
      vim = "nvim";
    };
  };

  programs = {
    zsh = {
      enable = true;
      interactiveShellInit = let
        curl = "${pkgs.curl}/bin/curl";
        install_antigen = pkgs.writeText "install-antigen.zsh" ''
          #!${pkgs.zsh}/bin/zsh

          antigen="$HOME/.antigen/antigen.zsh"
          url="https://cdn.rawgit.com/zsh-users/antigen/v1.3.1/bin/antigen.zsh"
          checksum="a37f5165f41dd1db9d604e8182cc931e3ffce832cf341fce9a35796a5c3dcbb476ed7d6e6e9c8c773905427af77dbe8bdbb18f16e18b63563c6e460e102096f3"

          installed() { return `[ -f $antigen ]` }

          if installed; then
            . $antigen
          else
            echo "$antigen is missing. Installing..." >&2
            ${curl} $url > /tmp/antigen.zsh

            if ! `echo "$checksum /tmp/antigen.zsh" | sha512sum -c --status`; then
              echo "Abort: wrong sha512 checksum!" >&2
              echo "downloaded from: $url" >&2
              echo "Expected sha512: $checksum" >&2
              echo "Actual   sha512: `sha512sum /tmp/antigen.zsh | cut -d ' ' -f 1`" >&2
              rm -f /tmp/antigen.zsh
            else
              mkdir -p `dirname $antigen`
              mv /tmp/antigen.zsh $antigen
              echo "Installed antigen." >&2
            fi

            if ! installed; then
              echo "Failed to install antigen!" >&2
            else
              . $antigen
            fi
          fi
        '';
      in ". ${install_antigen}";
    };
    mosh.enable = true;
    tmux.enable = true;
    adb.enable = true;
    light.enable = true;
  };

  # powerManagement.enable = true;

  services = {
    # acpid = {
    #   enable = true;
    #   lidEventCommands = "pm-suspend";
    # };
    locate.enable = true;
    openssh.enable = true;
    printing = {
      enable = false;
      drivers = [ pkgs.hplip ];
    };
  };

  services.thinkfan = {
    enable = true;
    sensor = "/sys/class/thermal/thermal_zone0/temp";
  };

  services.xserver = {
    enable = true;
    layout = "de";
    xkbOptions = "eurosign:e";

    videoDrivers = [ "Intel" ];

    synaptics = {
      enable = true;
      twoFingerScroll = true;
      palmDetect = true;
      minSpeed = "0.825";
      maxSpeed = "2";
    };

    displayManager = {
      slim.enable = true;
      slim.defaultUser = "suolrihm";
      sessionCommands = ''
    	cbatticon &
        compton -bfD 4
        dunst &
        ~/.fehbg
        nm-applet &
        blueman-applet &
        devmon &
        volumeicon &
        xflux -l 51.165691 -g 10.45152000000058
      '';
    };

    desktopManager.xterm.enable = false;

    windowManager = {
      default = "i3";
      i3 = {
        enable = true;
        package = pkgs.i3-gaps;
      };
    };
  };

  virtualisation.docker = {
    enable = true;
    enableOnBoot = false;
  };
  
  fonts = {
    enableFontDir = true;
    enableGhostscriptFonts = true;
    fonts = with pkgs; [
      anonymousPro
      corefonts
      dejavu_fonts
      fira
      fira-code
      fira-mono
      font-awesome-ttf
      inconsolata
      liberation_ttf
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      powerline-fonts
      terminus_font
      terminus_font_ttf
      ubuntu_font_family
      unifont
      unifont_upper
      unscii
      vistafonts
      wqy_zenhei
    ];
  };

  hardware = {
    enableAllFirmware = true;

    cpu.intel.updateMicrocode = true;

    bluetooth.enable = true;

    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
      extraPackages = [ pkgs.vaapiIntel ];
    };

    pulseaudio = {
      enable = true;
      package = pkgs.pulseaudioFull;
      support32Bit = true;
    };
  };

}